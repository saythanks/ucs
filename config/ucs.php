<?php

return [
    'api' => [
        'host' => env('UCS_API_HOST', 'https://api.ucs.services.thumbtribe.co.za/api/v1'),
        'pool' => env('UCS_API_POOL', ''),
        'username' => env('UCS_API_USERNAME', ''),
        'password' => env('UCS_API_PASSWORD', ''),
    ]
];
