swagger: '2.0'
info:
  title: UCS
  version: v1
  description: The Unified Coupon System API provides a standard interface for listing and issuing virtual coupons available at various retailers
host: api.ucs.services.thumbtribe.co.za
basePath: /api/v1
schemes:
 - https
consumes:
 - application/json
produces:
 - application/json
securityDefinitions:
  httpBasicAuth:
    type: basic
security:
  - httpBasicAuth: []
paths:
  '/{pool}/campaigns':
    get:
      summary: Retrieve all the currently available campaigns
      operationId: getCampaigns
      parameters:
        - name: pool
          in: path
          description: The name of the pool to use
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/CampaignResponse'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/{pool}/campaigns/query':
    post:
      summary: Query the currently available campaigns with the given user and filters
      operationId: queryCampaigns
      parameters:
        - in: body
          name: query
          description: The query critera to apply
          required: true
          schema:
            $ref: '#/definitions/CampaignQuery'
        - name: pool
          in: path
          description: The name of the pool to use
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/CampaignResponse'
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/{pool}/coupons/allocate':
    post:
      summary: Reserve coupons for a user for the given campaigns at the given retailers
      operationId: allocateCoupons
      parameters:
        - in: body
          name: allocateRequest
          description: The coupon reservation request
          required: true
          schema:
            $ref: '#/definitions/AllocateRequest'
        - name: pool
          in: path
          description: The name of the pool to use
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/AllocateResponse'
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/{pool}/coupons/deallocate':
    post:
      summary: Cancel reserved coupons
      operationId: deallocateCoupons
      parameters:
        - in: body
          name: deallocateRequest
          description: The reserved coupon cancellation request
          required: true
          schema:
            $ref: '#/definitions/DeallocateRequest'
        - name: pool
          in: path
          description: The name of the pool to use
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/DeallocateResponse'
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/{pool}/coupons/issue':
    post:
      summary: Generate redeemable codes at the retailers for reserved coupons
      operationId: issueCoupons
      parameters:
        - in: body
          name: issueRequest
          description: The reserved coupon issuance request
          required: true
          schema:
            $ref: '#/definitions/IssueRequest'
        - name: pool
          in: path
          description: The name of the pool to use
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/IssueResponse'
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/{pool}/coupons/query':
    post:
      summary: Query coupons with the given user and filters
      operationId: queryCoupons
      parameters:
        - in: body
          name: query
          description: The query critera to apply
          required: true
          schema:
            $ref: '#/definitions/CouponQuery'
        - name: pool
          in: path
          description: The name of the pool to use
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/CampaignResponse'
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
definitions:
  AllocateRequest:
    type: object
    required:
      - userId
      - couponRequests
    properties:
      couponRequests:
        type: array
        items:
          $ref: '#/definitions/CouponRequest'
      userId:
        type: string
    title: AllocateRequest
  AllocateResponse:
    type: object
    required:
      - successful
    properties:
      allocatedUserCoupons:
        type: object
        additionalProperties:
          $ref: '#/definitions/UserCoupon'
      campaigns:
        type: object
        additionalProperties:
          $ref: '#/definitions/Campaign'
      errorMessage:
        type: string
      retailers:
        type: object
        additionalProperties:
          $ref: '#/definitions/Retailer'
      successful:
        type: boolean
    title: AllocateResponse
  Campaign:
    type: object
    required:
      - id
      - startDate
      - endDate
      - couponsOverallLimit
      - couponsOverallRemaining
      - couponsPerUserLimit
      - couponCountsForCurrentUser
    properties:
      callToAction:
        type: string
      couponCountsForCurrentUser:
        $ref: '#/definitions/CouponCount'
      couponsOverallLimit:
        type: integer
        format: int32
      couponsOverallRemaining:
        type: integer
        format: int32
      couponsPerUserLimit:
        type: integer
        format: int32
      endDate:
        type: string
        format: date-time
      id:
        type: string
      imageUrl:
        type: string
      name:
        type: string
      offerDescription:
        type: string
      retailerIds:
        type: array
        items:
          type: string
      specialInstructions:
        type: string
      startDate:
        type: string
        format: date-time
      tags:
        type: object
        additionalProperties:
          type: string
    title: Campaign
  CampaignQuery:
    type: object
    properties:
      retailerId:
        type: string
      tags:
        type: object
        additionalProperties:
          type: string
      userId:
        type: string
    title: CampaignQuery
  CampaignResponse:
    type: object
    required:
      - campaigns
      - retailers
      - userCoupons
    properties:
      campaigns:
        type: object
        additionalProperties:
          $ref: '#/definitions/Campaign'
      retailers:
        type: object
        additionalProperties:
          $ref: '#/definitions/Retailer'
      userCoupons:
        type: object
        additionalProperties:
          $ref: '#/definitions/UserCoupon'
    title: CampaignResponse
  CouponCount:
    type: object
    required:
      - allocated
      - deallocated
      - expired
      - issued
      - redeemed
      - remaining
      - total
    properties:
      allocated:
        type: integer
        format: int32
      deallocated:
        type: integer
        format: int32
      expired:
        type: integer
        format: int32
      issued:
        type: integer
        format: int32
      redeemed:
        type: integer
        format: int32
      remaining:
        type: integer
        format: int32
      total:
        type: integer
        format: int32
    title: CouponCount
  CouponQuery:
    type: object
    required:
      - userId
    properties:
      campaignIds:
        type: array
        items:
          type: string
      retailerIds:
        type: array
        items:
          type: string
      states:
        type: array
        items:
          type: string
          enum:
            - ALLOCATED
            - DEALLOCATED
            - ISSUED
            - REDEEMED
            - EXPIRED
      tags:
        type: object
        additionalProperties:
          type: string
      userId:
        type: string
    title: CouponQuery
  CouponRequest:
    type: object
    required:
      - campaignId
      - retailerId
      - quantity
    properties:
      campaignId:
        type: string
      quantity:
        type: integer
        format: int32
      retailerId:
        type: string
    title: CouponRequest
  DeallocateRequest:
    type: object
    required:
      - userId
      - couponRequests
    properties:
      couponRequests:
        type: array
        items:
          $ref: '#/definitions/CouponRequest'
      userId:
        type: string
    title: DeallocateRequest
  DeallocateResponse:
    type: object
    required:
      - successful
    properties:
      campaigns:
        type: object
        additionalProperties:
          $ref: '#/definitions/Campaign'
      deallocatedUserCoupons:
        type: object
        additionalProperties:
          $ref: '#/definitions/UserCoupon'
      errorMessage:
        type: string
      retailers:
        type: object
        additionalProperties:
          $ref: '#/definitions/Retailer'
      successful:
        type: boolean
    title: DeallocateResponse
  IssueRequest:
    type: object
    required:
      - userId
      - couponRequests
    properties:
      couponRequests:
        type: array
        items:
          $ref: '#/definitions/CouponRequest'
      userId:
        type: string
    title: IssueRequest
  IssueResponse:
    type: object
    required:
      - successful
    properties:
      campaigns:
        type: object
        additionalProperties:
          $ref: '#/definitions/Campaign'
      errorMessage:
        type: string
      retailerIssueResponses:
        type: array
        items:
          $ref: '#/definitions/RetailerIssueResponse'
      retailers:
        type: object
        additionalProperties:
          $ref: '#/definitions/Retailer'
      successful:
        type: boolean
    title: IssueResponse
  Retailer:
    type: object
    required:
      - id
    properties:
      id:
        type: string
      name:
        type: string
    title: Retailer
  RetailerIssueResponse:
    type: object
    required:
      - successful
    properties:
      couponCode:
        type: string
      couponResponseType:
        type: string
        enum:
          - NONE
          - COUPON_CODE
      errorMessage:
        type: string
      issuedUserCoupons:
        type: object
        additionalProperties:
          $ref: '#/definitions/UserCoupon'
      retailerId:
        type: string
      successful:
        type: boolean
    title: RetailerIssueResponse
  UserCoupon:
    type: object
    required:
      - id
      - campaignId
      - retailerId
      - state
      - allocatedDate
    properties:
      allocatedDate:
        type: string
        format: date-time
      campaignId:
        type: string
      deallocatedDate:
        type: string
        format: date-time
      expiredDate:
        type: string
        format: date-time
      id:
        type: string
      issuedDate:
        type: string
        format: date-time
      redeemedDate:
        type: string
        format: date-time
      retailerId:
        type: string
      state:
        type: string
        enum:
          - ALLOCATED
          - DEALLOCATED
          - ISSUED
          - REDEEMED
          - EXPIRED
    title: UserCoupon
