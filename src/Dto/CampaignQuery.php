<?php

namespace SayThanks\Ucs\Dto;

class CampaignQuery
{
    public function __construct(
        public string $retailerId,
        public array $tags,
        public string $userId,
    ){}
}
