<?php

namespace SayThanks\Ucs\Dto;

use Carbon\CarbonImmutable;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class Campaign
{
    public string $id;
    public string $name;
    public CarbonImmutable $startDate;
    public CarbonImmutable $endDate;
    public int $valueInCents;
    public string $offerDescription;
    public string $specialInstructions;
    public string $callToAction;
    public int $couponsOverallLimit;
    public int $couponsPerUserLimit;
    public int $couponsOverallRemaining;
    public CouponCount $couponCountsForCurrentUser;
    public Collection $retailers;
    public Collection $retailerIds;
    public array $tags;

    public function __construct($campaign)
    {
        $this->id = Arr::get($campaign, 'id');
        $this->name = Arr::get($campaign, 'name');
        $this->startDate = CarbonImmutable::parse(Arr::get($campaign, 'startDate'));
        $this->endDate = CarbonImmutable::parse(Arr::get($campaign, 'endDate'));
        $this->valueInCents = Arr::get($campaign, 'valueInCents');
        $this->offerDescription = Arr::get($campaign, 'offerDescription');
        $this->specialInstructions = Arr::get($campaign, 'specialInstructions');
        $this->callToAction = Arr::get($campaign, 'callToAction');
        $this->couponsOverallLimit = Arr::get($campaign, 'couponsOverallLimit');
        $this->couponsOverallRemaining = Arr::get($campaign, 'couponsOverallRemaining');
        $this->couponCountsForCurrentUser = new CouponCount(Arr::get($campaign, 'couponCountsForCurrentUser'));
        $this->retailerIds = collect(Arr::get($campaign, 'retailerIds'));
        $this->tags = Arr::get($campaign, 'tags');
    }
}
