<?php

namespace SayThanks\Ucs\Dto;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class Retailer
{
    public string $id;
    public string $name;
    public string $description;
    public string $logoUrl;
    public string $type;
    public Collection $campaigns;

    public function __construct($retailer)
    {
        $this->id = Arr::get($retailer, 'id');
        $this->name = Arr::get($retailer, 'name');
        $this->description = Arr::get($retailer, 'description');
        $this->logoUrl = Arr::get($retailer, 'logoUrl');
        $this->type = Arr::get($retailer, 'type');
    }
}
