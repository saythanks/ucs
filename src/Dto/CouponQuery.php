<?php

namespace SayThanks\Ucs\Dto;

class CouponQuery
{
    public function __construct(
        public array $campaignIds,
        public array $retailerIds,
        public array $states,
        public array $tags,
        public string $userId,
    ){}
}
