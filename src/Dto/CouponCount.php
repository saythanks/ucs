<?php

namespace SayThanks\Ucs\Dto;

use Illuminate\Support\Arr;

class CouponCount
{
    public int $allocated;
    public int $deallocated;
    public int $issued;
    public int $redeemed;
    public int $expired;
    public int $total;
    public int $remaining;

    public function __construct($couponsCountForCurrentUser)
    {
        $this->allocated = Arr::get($couponsCountForCurrentUser, 'allocated');
        $this->deallocated = Arr::get($couponsCountForCurrentUser, 'deallocated');
        $this->issued = Arr::get($couponsCountForCurrentUser, 'issued');
        $this->redeemed = Arr::get($couponsCountForCurrentUser, 'redeemed');
        $this->expired = Arr::get($couponsCountForCurrentUser, 'expired');
        $this->total = Arr::get($couponsCountForCurrentUser, 'total');
        $this->remaining = Arr::get($couponsCountForCurrentUser, 'remaining');
    }
}