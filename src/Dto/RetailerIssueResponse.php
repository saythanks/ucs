<?php

namespace SayThanks\Ucs\Dto;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class RetailerIssueResponse
{
    public bool $successful;
    public string $retailerId;
    public string $couponResponseType;
    public Collection $userCoupons;
    public string $couponCode;
    public string $errorMessage;

    public function __construct($retailerIssueResponse)
    {
        $this->successful = Arr::get($retailerIssueResponse, 'successful');
        if ($this->successful) {
            $this->retailerId = Arr::get($retailerIssueResponse, 'retailerId');
            $this->couponResponseType = Arr::get($retailerIssueResponse, 'couponResponseType');
            $this->couponCode = Arr::get($retailerIssueResponse, 'couponCode');
            $this->userCoupons = collect(Arr::get($retailerIssueResponse, 'issuedUserCoupons'))
                ->values()
                ->map(function($retailer) { return new UserCoupon($retailer); });
        } else {
            $this->errorMessage = Arr::get($retailerIssueResponse, 'errorMessage');
            Log::error('UCS Retailer Issue Response error: ' . $this->errorMessage);
        }
    }
}
