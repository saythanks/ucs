<?php

namespace SayThanks\Ucs\Dto;

use Illuminate\Support\Collection;

class CouponRequestContainer
{
    public function __construct(
        public Collection $couponRequests,
        public string $userId,
    ){}
}
