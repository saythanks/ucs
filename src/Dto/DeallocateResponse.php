<?php

namespace SayThanks\Ucs\Dto;

class DeallocateResponse extends AllocationResponse
{
    public const COUPON_TYPE = 'deallocatedUserCoupons';
}
