<?php

namespace SayThanks\Ucs\Dto;

class AllocateResponse extends AllocationResponse
{
    public const COUPON_TYPE = 'allocatedUserCoupons';
}
