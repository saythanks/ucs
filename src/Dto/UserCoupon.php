<?php

namespace SayThanks\Ucs\Dto;

use Carbon\CarbonImmutable;
use Illuminate\Support\Arr;

class UserCoupon
{
    public string $id;
    public string $campaignId;
    public string $retailerId;
    public string $state;
    public CarbonImmutable $allocatedDate;
    public CarbonImmutable $deallocatedDate;
    public CarbonImmutable $issuedDate;
    public CarbonImmutable $redeemedDate;
    public CarbonImmutable $expiredDate;

    public function __construct($userCoupon)
    {
        $this->id = Arr::get($userCoupon, 'id');
        $this->campaignId = Arr::get($userCoupon, 'campaignId');
        $this->retailerId = Arr::get($userCoupon, 'retailerId');
        $this->state = Arr::get($userCoupon, 'state');
        if (isset($userCoupon['allocatedDate'])) {
            $this->allocatedDate = CarbonImmutable::parse(Arr::get($userCoupon, 'allocatedDate'));
        }
        if (isset($userCoupon['deallocatedDate'])) {
            $this->deallocatedDate = CarbonImmutable::parse(Arr::get($userCoupon, 'deallocatedDate'));
        }
        if (isset($userCoupon['issuedDate'])) {
            $this->issuedDate = CarbonImmutable::parse(Arr::get($userCoupon, 'issuedDate'));
        }
        if (isset($userCoupon['redeemedDate'])) {
            $this->redeemedDate = CarbonImmutable::parse(Arr::get($userCoupon, 'redeemedDate'));
        }
        if (isset($userCoupon['expiredDate'])) {
            $this->expiredDate = CarbonImmutable::parse(Arr::get($userCoupon, 'expiredDate'));
        }
    }
}
