<?php

namespace SayThanks\Ucs\Dto;

class CouponRequest
{
    public function __construct(
        public string $campaignId,
        public int $quantity,
        public string $retailerId,
    ){}
}
