<?php

namespace SayThanks\Ucs\Dto;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use SayThanks\Ucs\Exceptions\IssueCouponException;

class IssueResponse
{
    public Collection $campaigns;
    public Collection $retailers;
    public Collection $retailerIssueResponses;
    public string $errorMessage;
    public bool $successful;

    public function __construct($issueResponse, $issueRequest)
    {
        $this->successful = Arr::get($issueResponse, 'successful');
        if ($this->successful) {
            $this->campaigns = collect(Arr::get($issueResponse, 'campaigns'))
                ->values()
                ->map(function($campaign) { return new Campaign($campaign); });
            $this->retailers = collect(Arr::get($issueResponse, 'retailers'))
                ->values()
                ->map(function($retailer) { return new Retailer($retailer); });
            $this->retailerIssueResponses = collect(Arr::get($issueResponse, 'retailerIssueResponses'))
                ->values()
                ->map(function($retailer) { return new RetailerIssueResponse($retailer); });
        } else {
            $this->errorMessage = Arr::get($issueResponse, 'errorMessage');
            Log::error('UCS Issue token error: ' . $this->errorMessage, [
                'payload' => json_encode($issueRequest),
                'response' => $issueResponse,
            ]);
            throw new IssueCouponException($this->errorMessage, 0, null, $issueRequest);
        }
    }
}
