<?php

namespace SayThanks\Ucs\Dto;

use Illuminate\Support\Collection;

class CampaignResponse
{
    public Collection $campaigns;
    public Collection $retailers;

    public function __construct($campaignResponse)
    {
        $this->campaigns = collect($campaignResponse['campaigns'])
            ->values()
            ->map(function($campaign) { return new Campaign($campaign); });
        $this->retailers = collect($campaignResponse['retailers'])
            ->values()
            ->map(function($retailer) { return new Retailer($retailer); });
    }
}
