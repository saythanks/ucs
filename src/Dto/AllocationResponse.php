<?php

namespace SayThanks\Ucs\Dto;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use SayThanks\Ucs\Exceptions\AllocateCouponException;

class AllocationResponse
{
    public const COUPON_TYPE = '';

    public Collection $userCoupons;
    public Collection $campaigns;
    public Collection $retailers;
    public string $errorMessage;
    public bool $successful;

    public function __construct($allocationResponse, $allocationRequest)
    {
        $this->successful = Arr::get($allocationResponse, 'successful');
        if ($this->successful) {
            $this->userCoupons = collect(Arr::get($allocationResponse, self::COUPON_TYPE))
                ->values()
                ->map(function($coupon) { return new UserCoupon($coupon); });
            $this->campaigns = collect(Arr::get($allocationResponse, 'campaigns'))
                ->values()
                ->map(function($campaign) { return new Campaign($campaign); });
            $this->retailers = collect(Arr::get($allocationResponse, 'retailers'))
                ->values()
                ->map(function($retailer) { return new Retailer($retailer); });
        } else {
            $this->errorMessage = Arr::get($allocationResponse, 'errorMessage');
            Log::error('UCS Allocate token error: ' . $this->errorMessage, [
                'payload' => json_encode($allocationRequest),
                'response' => $allocationResponse,
            ]);
            throw new AllocateCouponException($this->errorMessage, 0, null, $allocationRequest);
        }
    }
}
