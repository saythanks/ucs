<?php

namespace SayThanks\Ucs\Facades;

use Illuminate\Support\Facades\Facade;

class UcsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ucs';
    }
}
