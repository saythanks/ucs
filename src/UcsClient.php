<?php

namespace SayThanks\Ucs;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use SayThanks\Ucs\Dto\AllocateRequest;
use SayThanks\Ucs\Dto\AllocateResponse;
use SayThanks\Ucs\Dto\CampaignQuery;
use SayThanks\Ucs\Dto\CampaignResponse;
use SayThanks\Ucs\Dto\CouponQuery;
use SayThanks\Ucs\Dto\CouponRequest;
use SayThanks\Ucs\Dto\DeallocateRequest;
use SayThanks\Ucs\Dto\DeallocateResponse;
use SayThanks\Ucs\Dto\IssueRequest;
use SayThanks\Ucs\Dto\IssueResponse;

final class UcsClient
{
    public PendingRequest $client;

    public function __construct(string $pool, string $username, string $password)
    {
        $this->client = Http::retry(3, 100)
            ->baseUrl(implode('/', [config('ucs.api.host'), $pool]))
            ->asJson()
            ->acceptJson()
            ->withHeaders([
                'Authorization' => 'Basic ' . base64_encode($username . ':' . $password),
            ]);
    }

    public function campaigns()
    {
        return new CampaignResponse($this->get('campaigns')->json());
    }

    public function queryCampaigns(CampaignQuery $campaignQuery)
    {
        return new CampaignResponse($this->post('campaigns/query', (array) $campaignQuery)->json());
    }

    public function queryCoupons(CouponQuery $couponQuery)
    {
        return new CampaignResponse($this->post('coupons/query', (array) $couponQuery)->json());
    }

    public function allocateCoupons(AllocateRequest $allocateRequest)
    {
        return new AllocateResponse($this->post('coupons/allocate', (array) $allocateRequest)->json(), $allocateRequest);
    }

    public function deallocateCoupons(DeallocateRequest $deallocateRequest)
    {
        return new DeallocateResponse($this->post('coupons/deallocate', (array) $deallocateRequest)->json(), $deallocateRequest);
    }

    public function issueCoupons(IssueRequest $issueRequest)
    {
        return new IssueResponse($this->post('coupons/issue', (array) $issueRequest)->json(), $issueRequest);
    }

    public function getCoupon(string $campaignId, string $retailerId, string $userId, int $quantity)
    {
        $couponRequest = collect([new CouponRequest($campaignId, $quantity, $retailerId)]);
        $allocateRequest = new AllocateRequest($couponRequest, $userId);
        $allocateResponse = $this->allocateCoupons($allocateRequest);
        $issueRequest = new IssueRequest($couponRequest, $userId);
        $issueResponse = $this->issueCoupons($issueRequest, $userId);
        return $issueResponse->retailerIssueResponses->first();
    }

    public function getCouponCode(string $campaignId, string $retailerId, string $userId, int $quantity)
    {

        return $this->getCoupon($campaignId, $retailerId, $userId, $quantity)->couponCode;
    }

    private function get(string $url, array $queryData = []): \GuzzleHttp\Promise\PromiseInterface|Response
    {
        return $this->request('get', $url, $queryData);
    }

    private function post(string $url, $data): \GuzzleHttp\Promise\PromiseInterface|Response
    {
        return $this->request('post', $url, $data);
    }

    private function request(string $method, string $url, $data = []): \GuzzleHttp\Promise\PromiseInterface|Response
    {
        if (!in_array($method, ['get', 'post'])) {
            throw new \InvalidArgumentException("UcsClient: unsupported request type $method specified - expected 'get' or 'post'.");
        }

        Log::info("UcsClient: Sending $method request", ['url' => $url, 'query' => $data]);
        /** @var \GuzzleHttp\Promise\PromiseInterface|Response $response */
        $response = $this->client->$method($url, $data);

        $logData = [
            'url' => $url,
            'data' => $data,
            'response_code' => $response->status(),
            'response_json' => $response->json(),
        ];
        if ($response->successful()) {
            Log::debug("UcsClient: $method request successful", $logData);
        } else {
            Log::warning("UcsClient: $method request unsuccessful", $logData);
        }
        return $response;
    }
}
