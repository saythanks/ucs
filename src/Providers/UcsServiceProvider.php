<?php

namespace SayThanks\Ucs\Providers;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider;
use SayThanks\Ucs\UcsClient;

class UcsServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/ucs.php',
            'ucs'
        );

        $this->app->bind(
            'ucs',
            static function ($app, $params) {
                config('ucs.api.pool');
                config('ucs.api.username');
                config('ucs.api.password');
                $pool = $params['pool'] ?? config('ucs.api.pool');
                $username = $params['username'] ?? config('ucs.api.username');
                $password = $params['password'] ?? config('ucs.api.password');

                if ($username === null) {
                    throw new BindingResolutionException(
                        'Cannot create UCS Client: Pool is missing.
                        Please ensure UCS_API_POOL is in your .env or provide a username when creating the client.'
                    );
                }

                if ($username === null) {
                    throw new BindingResolutionException(
                        'Cannot create UCS Client: Username is missing.
                        Please ensure UCS_API_USERNAME is in your .env or provide a username when creating the client.'
                    );
                }

                if ($password === null) {
                    throw new BindingResolutionException(
                        'Cannot create UCS Client: Password is missing.
                        Please ensure UCS_API_PASSWORD is in your .env or provide a password when creating the client.'
                    );
                }

                return new UcsClient($pool, $username, $password);
            }
        );
    }
}
