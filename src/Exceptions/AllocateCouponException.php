<?php

namespace SayThanks\Ucs\Exceptions;

use Exception;
use Throwable;

class AllocateCouponException extends Exception
{
    public $payload;

    public function __construct($message = '', $code = 0, Throwable $previous = null, $payload)
    {
        $this->payload = $payload;
        parent::__construct($message, $code, $previous);
    }
}
